package hu.hb.evolution;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JPanel;

public class Panel extends JPanel {

    private World world;

    public Panel(World w) {
        world = w;
    }

    @Override
    protected void paintComponent(Graphics gg) {
        Graphics2D g = (Graphics2D) gg;
        
        double ratio1 = ((double)getWidth()) / World.WIDTH,
                ratio2 = ((double)getHeight()) / World.HEIGHT,
                ratio;
        if(ratio1 < ratio2) {
            ratio = ratio1;
        } else {
            ratio = ratio2;
        }
        g.clearRect(0, 0, this.getWidth(), this.getHeight());
        g.scale(ratio, ratio);
        
        
        g.setColor(new Color(0xcccccc));
        g.fillRect(0,0, World.WIDTH, World.HEIGHT);
        
        for (Creature c : world.creatures) {
            int x = (int) c.pos.x;
            int y = (int) c.pos.y;
            int r = (int) c.size;
            g.setColor(new Color(c.color));
            g.fillOval(x-r, y-r, 2*r, 2*r);
        }
    }

}
