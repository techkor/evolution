package hu.hb.evolution;

public class Position {

    public double x, y;

    public double distance(Position p) {
        double dx = x - p.x;
        double dy = y - p.y;
        return Math.sqrt(dx * dx + dy * dy);
    }
}
