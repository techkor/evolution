package hu.hb.evolution;

public class Creature extends Thing {
    public double size;
    public double speed; // Sebesség nagysága
    public double direction; // Sebesség iránya fokban
    public int color;
    
    
    public void eat(Creature creature) {
        size = Math.sqrt(size*size + creature.size*creature.size);
        creature.die();
    }
    
    public void eat(Food food) {
        this.size += food.value;
        food.die();
    }
    
    public void changeDirection() {
        
    }
}
