package hu.hb.evolution;

public class Thing {
    public Position pos;
    public boolean dead = false;
    
    public void die() {
        dead = true;
    }
}
