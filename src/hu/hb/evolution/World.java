package hu.hb.evolution;

import java.awt.Color;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class World {
    
    public static final int WIDTH = 1000, HEIGHT = 600;

    public Set<Creature> creatures = new HashSet<>();
    public Set<Food> foods = new HashSet<>();
    
    public Player player;

    public void startGame() {
        int num = (int) genRandom(5, 10);
        for (int i = 0; i < num; i++) {
            addNewCreature();
        }
        player = new Player();
        player.pos = new Position();
        player.pos.x = genRandom(0, WIDTH);
        player.pos.y = genRandom(0, HEIGHT);
        player.color = 0xff0000;
        player.size = 30;
        player.speed = 150;
        creatures.add(player);
    }

    public void step(double dt) {
        moveCreatures(dt);
        eatCreatures();
        eatFoods();
        deleteDeadThings();
    }

    public void addNewCreature() {
        Creature c = new Bot();
        c.pos = new Position();
        c.pos.x = genRandom(0, WIDTH);
        c.pos.y = genRandom(0, HEIGHT);
                
        c.size = genRandom(10, 40);
        c.speed = genRandom(100, 200);
        c.direction = genRandom(0, 2 * Math.PI);
        c.color = (int) genRandom(0, 0xffffff);
        creatures.add(c);
    }

    private double genRandom(double min, double max) {
        return Math.random() * (max - min) + min;
    }

    private void moveCreatures(double dt) {
        for (Creature c : creatures) {
            c.changeDirection();
            c.pos.x += c.speed * dt * Math.cos(c.direction);
            c.pos.y += c.speed * dt * Math.sin(c.direction);
            
            if(c.pos.x < c.size)
                c.pos.x = c.size;
            else if(c.pos.x > WIDTH - c.size)
                c.pos.x = WIDTH - c.size;
            
            if(c.pos.y < c.size)
                c.pos.y = c.size;
            else if(c.pos.y > HEIGHT - c.size)
                c.pos.y = HEIGHT - c.size;
        }
    }

    private void eatCreatures() {
        for (Creature c1 : creatures) {
            for (Creature c2 : creatures) {
                if (!c1.dead && !c2.dead
                        && c1.pos.distance(c2.pos) < c1.size + c2.size
                        && c1.size < c2.size) {
                    c2.eat(c1);
                }
            }
        }
    }

    private void eatFoods() {
        for (Creature c : creatures) {
            for (Food f : foods) {
                if (!c.dead && !f.dead
                        && c.pos.distance(f.pos) < c.size) {
                    c.eat(f);
                }
            }
        }
    }

    private void deleteDeadThings() {
        for (Iterator<Creature> it = creatures.iterator(); it.hasNext();) {
            Creature c = it.next();
            if (c.dead) {
                it.remove();
            }
        }
        for (Iterator<Food> it = foods.iterator(); it.hasNext();) {
            Food f = it.next();
            if (f.dead) {
                it.remove();
            }
        }
    }
}
